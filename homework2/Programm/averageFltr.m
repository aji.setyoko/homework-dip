function [ outputImg ] = averageFltr( img ,filterSize)
%AVERAGEFLTR Summary of this function goes here
%   Detailed explanation goes here
close()
orig = img;
img = double(img);
[w h] =size(img);
newImg= zeros(w,h);
convWindow = ones(filterSize,filterSize);
sizeWindow = size(convWindow);
denom = sizeWindow(1)*sizeWindow(2);
img_pad = padarray(img,[floor(filterSize/2) floor(filterSize/2)],'replicate','both');
for i = 1 : w
    for ii = 1 : h
        %Loop to get sub matrix image
        sub_img = img_pad((i:i+filterSize-1),(ii:ii+filterSize-1));
        outputConv = convManual(sub_img,convWindow);
        newPixel= (outputConv/denom);
        newImg(i,ii)= newPixel;
    end
end
outputImg = uint8(newImg);
%saving figure
imshow(outputImg);
title2= strcat('Average ',int2str(filterSize),' Kernel Size','.png');
saveas(gcf,title2);
close(gcf);
%Plotting Figure 
subplot(1,2,1);
imshow(orig);
title('Original Image')
subplot(1,2,2);
imshow(outputImg);
title2= strcat(int2str(filterSize),' Kernel Size');
title({'After Average Filtering with '; title2 })


end

