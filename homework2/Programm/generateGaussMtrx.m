function [ f ] = generateGaussMtrx( N,sigma)
%GENERATEGAUSSMTRX Summary of this function goes here
%   Detailed explanation goes here
%y = (1/sqrt(2*pi*(sigma*sigma)))*exp(-((x).^2)/(2*sigma.^2));
[x y]=meshgrid(round(-N/2)+1:floor(N/2), round(-N/2)+1:floor(N/2));
f=exp(-x.^2/(2*sigma^2)-y.^2/(2*sigma^2));
f=f./sum(f(:));
end
