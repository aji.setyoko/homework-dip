# homework-DIP

Digital Image Processing Homework Repository
Every Programm write on Matlab Function (.m)

Added Program :
##  Week 1:  
-                Local Image Enhancment
-                Global Image Enhancement
-                Manual histogram solver
##   Week 2:  
-                Gaussian Filter
-                Average Filter
-                Median Filter
-                Manual Convolution
-                Manual Kernel Gauss Matrix Generator
##   Week 3:
-                2D DCT 
-                2D DCT-Inverse
-                1D DCT
-                1D DCT-Inverse