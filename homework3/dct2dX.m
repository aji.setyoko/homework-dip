function [ result ] = dct2dX( imgMtx ,blokSize)
%DCT2D Summary of this function goes here
%   Detailed explanation goes here
%dctMtx = dctmtxgen(size(imgMtx,1));
dctMtx = dctmtxgen(blokSize);
%Using matrix calculation, 2dDCT(im) = dctMtx*im*dctMtx'
result = zeros(size(imgMtx,1));
ii = 1;
x = size(imgMtx,1)/blokSize;
for a = 1:x
    i  = 1;
    for b = 1:x
        blok =imgMtx(ii:ii+blokSize-1,i:i+blokSize-1);
        prosImg = dctMtx*blok*dctMtx';
        result(ii:ii+blokSize-1,i:i+blokSize-1) = prosImg;
        i = i+blokSize;
    end
    ii = ii+blokSize;
end
end

