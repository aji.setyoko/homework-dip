function [ result ] = oneDCT( imgMtx )
%ONEDCT Summary of this function goes here
%   Detailed explanation goes here
blokSize = 8;
x = size(imgMtx,1)/blokSize;
ii = 1;
for a = 1:x
    i  = 1;
    for b = 1:x
        blok =imgMtx(ii:ii+blokSize-1,i:i+blokSize-1);
        resultHor = onedGenMtxHor(blok);
        resultVer = onedGenMtxHor(resultHor');
        result(ii:ii+blokSize-1,i:i+blokSize-1) = resultVer;
        i = i+blokSize;
    end
    ii = ii+blokSize;
end
end

