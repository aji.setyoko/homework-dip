function [ newBlok ] = onedGenMtxHor(blok)
N = 8;
i = 1:8;
u = i-1;
newBlok = zeros(8);
YY =0;
YOut(i) = 0;
for b = 1:8
    f = blok(b,:);
    for a = 1:8
        x = u(a);
        YY =0;
        for i = 1:N
            YY = YY + (cos(pi*((x)/(2*N)*(2*(i-1)+1)))*f(i));
        end
        if x==0
            YY = sqrt(2/N)*(sqrt(2)/2)*YY;
        else
            YY = sqrt(2/N)*(1)*YY;
        end
        YOut(a) = YY;
    end
    newBlok(b,:)=YOut;
end
end




% result = zeros(8);
% i = 1:8;
% freqDomain(i) = 0;
% for x = 1 : 8
%     fmx = blok(x,:);
%     for i = 1:8
%         u = i-1;
%         if u == 0
%             freqDomain(x,i) = sqrt(1/8)*fmx(i)*cos((2*i+1)*u*pi/16);
%         else 
%             freqDomain(x,i) = sqrt(2/8)*fmx(i)*cos((2*i+1)*u*pi/16);
%         end
%     end
%     result(x,:) = freqDomain(x,:);

