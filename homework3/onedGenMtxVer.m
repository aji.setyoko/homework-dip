function [ result ] = onedGenMtxVer(blok)
result = zeros(8);
i = 1:8;
freqDomain(i) = 0;
for x = 1 : 8
    fmx = blok(:,x);
    fmx = fmx';
    for i = 1:8
        u = i-1;
        if u == 0
            freqDomain(x,i) = sqrt(1/8)*fmx(i)*cos((2*i+1)*u*pi/16);
        else 
            freqDomain(x,i) = sqrt(2/8)*fmx(i)*cos((2*i+1)*u*pi/16);
        end
    end
    result(:,x) = freqDomain(x,:)';
end

