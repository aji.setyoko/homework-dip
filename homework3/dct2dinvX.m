function [ result ] = dct2dinvX( imgMtx,blokSize )
%DCT2DINVX Summary of this function goes here
%   this function is use to make a transformation from freq. domain to
% Spatial domain using inverse 2dDct.

%dctMtx = dctmtxgen(size(imgMtx,1));

% Using matrix transformation to get the inverse matrix for inverse.
% dctMtx is the tranformation matrix to tranform an matrix from spatial 
% to freq. So to get back to the original simply using the inverse matrix
% of dctMtx, which this matrix can be get by, dctMtx * dctMtx' = I

% inversedctMtx = inv(dctMtx);

%using the same formula to compute the inverse of the input matrix
% prosImgInverse = inversedctMtx*imgMtx*inversedctMtx';

dctMtx = dctmtxgen(blokSize);
%Using matrix calculation, 2dDCT(im) = dctMtx*im*dctMtx'
inversedctMtx = inv(dctMtx);
result = zeros(size(imgMtx,1));
ii = 1;
x = size(imgMtx,1)/blokSize;
for a = 1:x
    i  = 1;
    for b = 1:x
        blok =imgMtx(ii:ii+blokSize-1,i:i+blokSize-1);
        prosImg = inversedctMtx*blok*inversedctMtx';
        result(ii:ii+blokSize-1,i:i+blokSize-1) = prosImg;
        i = i+blokSize;
    end
    ii = ii+blokSize;
end

end

