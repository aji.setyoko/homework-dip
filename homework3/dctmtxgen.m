function [ valueX ] = dctmtxgen( numPixel )
%DCTMTXGEN Summary of this function goes here
%   Manual DCT matrixgeneratir, input consist of size of the image input


% make a two dummy matrix, consist of increased value by 1 every column
% and increased value by 1 every row. invreased until number of pixel -1
[col,rrow] = meshgrid(0:numPixel-1);

% fed to 2d dct formula

valueX = sqrt(2 / numPixel) * cos(pi * (2*col + 1) .* rrow / (2 * numPixel));
valueX(1,:) = valueX(1,:) / sqrt(2);

end

