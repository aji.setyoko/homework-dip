function [output] = localEnhanment( input,kernelSize )
%Documentation
% If we look to a course slide page 35 ; 
% chapter 4 Image Enhancement in the Spatial Domain. 
% There are 3 main parameter, 
%       ri     --> unique bit in the image
%       number --> counts of each unique bit
%       sum    --> total adding of sorted counts of each unique bit.
%       si     --> new unique bit(pixel) for every unique 
%                  bit(pixel) on original image
% Then in this program there are another main parameter
%       sum_spesofif_matrix  --> This variable corespondense to center pixel
%                                that will be change to new pixel.
%       multiplier factor    --> numerator and denumerator to get the new
%                                pixel
lclPixel = kernelSize;
    %size matrix to make an enhancement
sizePad = floor(lclPixel/2);
image_pad = padarray(input,[sizePad sizePad],'replicate');
    %Adding replication outer pixel with total 3 to every direction. To
    %make image after local enhancement have same size with the original
    %With kernel local size is kernelSize x kernelSize
[w,h] = size(input);
    %Get original image size for looping and sliding the kernel
image_result = input;
    % Initialize parameter for output and sum_spesific_matrix
sum_spesific_matrix = 0;
    % Looping to slide the kernel to get sum_spesifix_matrix and new pixel
    % value after local enhancement
for i =1:w
    for ii = 1:h
    copy_kernel = image_pad((i:i+lclPixel-1),(ii:ii+lclPixel-1)); 
    % fill kernel with image value
    [C,ia,ic] = unique(copy_kernel); 
    % find every unique pixel value from kernal matrix // ri-->C
    a_counts = accumarray(ic,1);
    % find counts of unique value in the kernel matrix//Number-->a_counts
    value_counts = [C, a_counts];
    % matrix with value unique pixel and their counts in the next coloum
    numerator = 255;
    % get the numerator for multipier factor
    spesific_matrix = copy_kernel((lclPixel+1)/2,(lclPixel+1)/2);
    % get the local pixel value which want to change, always in the middle
    % of the kernel.
    sum_prev = 0;sum_now=0; 
    % initialization variable for calculate ---> sum
    % Do Looping to get Denumerator for multipler Factor
    for iii=1:length(a_counts)
        sum_now = sum_prev+a_counts(iii);
        if value_counts(iii,1)==spesific_matrix
            sum_spesific_matrix = sum_now;
        end
        sum_prev = sum_now;
    end
    % Pixel Recontruction with local enhancements
    image_result(i,ii) = sum_spesific_matrix/sum_now*numerator;
    end
end
%Showing the result
%imshow(image_result);
output = image_result;
end

