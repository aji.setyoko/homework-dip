function [  ] = histogramEq( input )
%Documnetation ,
%This fucntion for call function to local and global histogram
%equilization, and do a plotting in one window. Here can be set the window
%size to local equilization function.
kernelSize = 7;%must be odd
localEq = localEnhanment(input,kernelSize);
globEq = globalhistogramEqulization(input);

subplot(2,3,1);
imshow(input);
title('Original');
subplot(2,3,2);
imshow(globEq);
title('Global Histogrm Eq.');
subplot(2,3,3);
imshow(localEq);
titleX = strcat('with Kernel Size',int2str(kernelSize));
title({'Local Histogram Eq.',titleX});
%Showing Histogram
subplot(2,3,4);
[pixel,count] = showingHistogram(input);
bar(pixel,count);
xlim([0 255]);
title('Original Histogram');

subplot(2,3,5);
[pixel,count] = showingHistogram(globEq);
bar(pixel,count);
xlim([0 255]);
title({'Histogram After Global','Histogram Eq.'});

subplot(2,3,6);
[pixel,count] = showingHistogram(localEq);
bar(pixel,count);
xlim([0 255]);
title({'Histogram After Local','Histogram Eq.'});

saveas(gcf,'university.png');
clear;clc;
end

