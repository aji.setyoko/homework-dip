function [output] = globalhistogramEqulization( image )
%Documentation
% If we look to a course slide page 35 ; 
% chapter 4 Image Enhancement in the Spatial Domain. 
% There are 3 main parameter, 
%       ri     --> unique bit in the image
%       number --> counts of each unique bit
%       sum    --> total adding of sorted counts of each unique bit.
%       si     --> new unique bit(pixel) for every unique 
%                  bit(pixel) on original image
% Then in this program there are another main parameter
%       sum_spesofif_matrix  --> This variable corespondense to center pixel
%                                that will be change to new pixel.
%       multiplier factor    --> numerator and denumerator to get the new
%                                pixel
M = max(image(:));
%The highest number in the image
hist_level = zeros(1,M+1);
%Matrix from zero to the highest level of the image --> all is zero
sequenc = (1:M+1);
%Matrix from zero to the highest level of the image --> 1 to highest number
[w,h] = size(image);
%size of the image
for i=1:w
    for ii=1:h
        lvl = image(i,ii);
        hist_level(lvl+1) = hist_level(lvl+1);
        %+1 because matlab index started from 1, so everything scaled
    end
end
%Manual looping to count unique number on each pixel on the image and count
%it; its identif with unique function on the matlab
sum =zeros(1,M+1);
sum_prev=0;
for i = 1:M+1
    sum(i)=hist_level(i)+sum_prev;
    sum_prev = sum(i);
end
%Loop to get the matrix sum --> Slide
equil_hist=zeros(1,M+1);
numerator_hist = 255;
%Numerator is 255, bcause the maximal level of graysclae is 255. On the
%slide is 7 which is the maximal number of gray level 3 bit. 
denominator_hist = max(sum(:));
for i = 1:M+1
    equil_hist(i) = sum(i)/denominator_hist*numerator_hist;
end
%Loop to get the matrix si --> Slide, a number that represent the new pixel
%value for every pixel on the image
matrix = [sequenc;equil_hist];
imge_after =zeros(w,h);
imge_after = uint8(imge_after);
for i=1:w
    for ii=1:h
        lvl = image(i,ii);
        lvl_after = matrix(2,lvl+1);
        imge_after(i,ii) = lvl_after;
    end
end
%Loop to recombine the pixel to be a new image
output = imge_after;
end

